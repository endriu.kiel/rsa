asgiref==3.4.1
backports.zoneinfo==0.2.1
beautifulsoup4==4.10.0
Django==4.0.1
django-bootstrap-v5==1.0.8
django-hosts==5.0
django-widget-tweaks==1.4.11
importlib-metadata==3.10.1
soupsieve==2.3.1
sqlparse==0.4.2
zipp==3.7.0
