from django.urls import path
from . import views



urlpatterns = [
    path("patient/", views.patient_list, name="patient-list"),
    path("patient/<str:pk>/", views.patient_detail, name="patient-detail"),
    path("htmx/search-patients/", views.search_patients, name="search-patients"),
    path("create-patient/", views.create_patient, name="create-patient"),
    path("calendar/", views.calendar, name="calendar"),
]
